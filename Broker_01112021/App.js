import React,{useEffect} from 'react'
import { Provider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import { theme } from './src/core/theme'
import { View, Text, Alert, StatusBar } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  SplashScreen,
  Dashboard,
  SetPasswordScreen,
  VerifyOtpScreen,
  ChangePasswordScreen,
  SearchSelectSeller,
  DealDetails,
  NegotiateDetails,
  NotificationSelectSeller,
  MyPostDetails,
  MyContractFilter,
  MyContractDetails,RequestScreen,
} from './src/screens'
import firebase from '@react-native-firebase/app';
import EditProfile from './src/components/EditProfile'
import ProfileSeen from './src/components/ProfileSeen'


import messaging from '@react-native-firebase/messaging';
import EncryptedStorage from 'react-native-encrypted-storage';
import Custom from './src/components/Custom'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { FirstRoute, SecondRoute, ThirdRoute } from './src/components/CalculatorView'
import { Appbar, Searchbar, Button, Badge } from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from './src/components/responsive-ratio';
const Stack = createStackNavigator()
const Tab = createMaterialTopTabNavigator();
const AppHeading = (props) => {
  console.log('props', props)
  return (
    <View style={{ width: '100%', height: hp(9), marginTop: hp(4) }}>
      <Appbar.Header style={{ backgroundColor: 'transparent' }}>
        {props.menu ? <Appbar.Action
          icon="menu"
          color="white"
          onPress={props.leftPress}
        /> :
          <Appbar.Action
            icon={() => <Ionicons name='chevron-back-outline' size={hp(3)} color='#fff' />}
            color="white"
            onPress={props.leftPress}
          />}
        <Appbar.Content
          style={{ alignItems: 'center' }}
          color="white"
          title={props.title}
          titleStyle={{ fontSize: 20, fontFamily: "Poppins-SemiBold" }}
        />
        {props.filter ? <Appbar.Action
          icon="notification-clear-all"
          color={"white"}
          onPress={
            props.rightPress
            // this.setState({ isFilterShow: true });
            // // if (this.state.isMyContracts || this.state.isProfile) {
            //   this.state.isMyContracts && this.props.navigation.navigate
            //     ('MyContractFilter', { productList: this.state.productItem });
            //   this.state.isProfile && this.props.navigation.navigate('EditProfile', { data: this.state.ProfileData })

          }
        /> : <Appbar.Action
          icon="menu"
          color="transparent"
          onPress={() => null}
        />}
      </Appbar.Header>
    </View>
  )
}

const Request = ({ navigation, route }) => {
  console.log('navigation>', navigation)
  return (
    <View style={{ flex: 1, backgroundColor: '#333' }}>
      <AppHeading title={'Requests'} menu leftPress={() => navigation.navigate('Dashboard')} />
      <View
        style={{
          width: '100%',
          // height: '86%',
          flex: 1,
          paddingBottom: 30,
          // marginTop: 10,
          backgroundColor: 'white',
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}>
        <RequestScreen navigation={navigation} route={route} />
      </View>
    </View>
  )
}

const ProfileSeenFunction = ({ navigation, route }) => {
  console.log('navigation>', navigation)
  return (
    <View style={{ flex: 1, backgroundColor: '#333' }}>
      <AppHeading title={'Profile'}  leftPress={() => navigation.goBack()} />
      <View
        style={{
          width: '100%',
          // height: '86%',
          flex: 1,
          paddingBottom: 30,
          // marginTop: 10,
          backgroundColor: 'white',
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}>
        <ProfileSeen navigation={navigation} route={route} />
      </View>
    </View>
  )
}

const tabnavi = ({ navigation }) => {
  console.log('nabvi', navigation)
  return (
    <View style={{ flex: 1, backgroundColor: '#333', }}>
      <View style={{
        flexDirection: 'row', paddingHorizontal: wp(5),
        marginTop: hp(2), height: hp(5), alignItems: 'center', justifyContent: 'space-between'
      }}>
        <Ionicons name='chevron-back-outline' size={hp(3)} color='#fff' style={{ width: wp(30) }} onPress={() => navigation.goBack()} />
        <Text style={{ alignSelf: 'center', color: '#fff', fontSize: hp(3), fontFamily: 'Poppins - Regular' }}>Calculator</Text>
        <View style={{ width: wp(30) }} />

      </View>
      <View style={{
        flex: 1,
        width: '100%',
        height: hp(86),
        paddingBottom: 30,
        paddingTop: hp(3),
        marginTop: hp(2),
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}>
        <Tab.Navigator tabBarOptions={{
          labelStyle: { fontSize: hp(2), fontFamily: 'Poppins - Regular' },
          activeTintColor: theme.colors.primary,
          inactiveTintColor: '#afafaf',
          indicatorStyle: { backgroundColor: theme.colors.primary }
        }}>
          <Tab.Screen name="Ginning" component={FirstRoute} />
          <Tab.Screen name="Spinning" component={SecondRoute} />
          <Tab.Screen name="Exports" component={ThirdRoute} />
        </Tab.Navigator>
      </View>
    </View>
  );
}
const App = () => {

  const credentials = {
    clientId: '1092885786732-mbkufs9gkblr44rd0ma0gosj1his7pj5.apps.googleusercontent.com',
    appId: '1:1092885786732:android:5c985918395a6ac6676849',
    apiKey: 'AIzaSyAh0zSYw6abqVxVzJjj9-KkOEknYqEYRfA',
    storageBucket: 'cotton-broker-25b0d.appspot.com',
    databaseURL: 'https://databasename.firebaseio.com',
    messagingSenderId: '1092885786732',
    projectId: 'cotton-broker-25b0d',
  };

  async function onAppBootstrap() {
    // Register the device with FCM

    if (!firebase.apps.length) {
      await firebase.initializeApp(credentials);
    }

    await messaging().registerDeviceForRemoteMessages();

    // Get the token
    const token = await messaging().getToken();

    console.log('token', token)

    await EncryptedStorage.setItem('FCMToken', token);

    // Save the token
  }


  async function onMessageReceived(message) {
    console.log('message', message)
  }

  useEffect(() => {
    onAppBootstrap()
    
    // const unsubscribe = messaging().onMessage(async remoteMessage => {
    //   Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    // });

    messaging().onMessage(onMessageReceived);
    messaging().setBackgroundMessageHandler(onMessageReceived);

    // return unsubscribe;
  }, []);

  return (
    <Provider theme={theme}>
      <NavigationContainer>
        
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen
            name="ForgotPasswordScreen"
            component={ForgotPasswordScreen}/>
            <Stack.Screen
            name="SetPasswordScreen"
            component={SetPasswordScreen}/>
            <Stack.Screen
            name="ChangePasswordScreen"
            component={ChangePasswordScreen}/>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="VerifyOtpScreen" component={VerifyOtpScreen} />
		  <Stack.Screen name="MyContractDetails" component={MyContractDetails} />
		  <Stack.Screen name="MyContractFilter" component={MyContractFilter} />
          <Stack.Screen name="SearchSelectSeller" component={SearchSelectSeller} />
          <Stack.Screen name="DealDetails" component={DealDetails} />
          <Stack.Screen name="NegotiateDetails" component={NegotiateDetails} />
          <Stack.Screen name="NotificationSelectSeller" component={NotificationSelectSeller} />
          <Stack.Screen name="MyPostDetails" component={MyPostDetails} />
          <Stack.Screen name="Custom" component={Custom} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="Calculator" component={tabnavi} />
          <Stack.Screen name="RequestScreen" component={Request} />
          <Stack.Screen name="ProfileSeen" component={ProfileSeenFunction} />



        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App
